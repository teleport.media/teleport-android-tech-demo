package com.teleport.demo

import android.app.Application
import com.teleport.sdk.TeleportSDK

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        TeleportSDK.init(this)
    }
}