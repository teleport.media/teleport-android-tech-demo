package com.teleport.demo

import android.media.session.PlaybackState
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.teleport.sdk.InitializationException
import com.teleport.sdk.TeleportSDK
import com.teleport.sdk.events.TeleportEvents
import com.teleport.sdk.interfaces.BufferSizeGetter
import com.teleport.sdk.model.PeeringMode
import com.teleport.sdk.model.stat.DownloadStatSegment
import com.teleport.sdk.model.stat.UploadStatSegment


class MainActivity : AppCompatActivity(), TeleportEvents, Player.Listener {

    private val TAG = "TeleportSDK"

    // Put Teleport api key here
    private val apiKey: String = ""
    // Put stream url here
    private val urlToPlay = Uri.parse("")

    private lateinit var playerView: StyledPlayerView
    private lateinit var player: ExoPlayer
    private val teleportSDK = TeleportSDK(apiKey)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        playerView = findViewById(R.id.playerView)
        player = ExoPlayer.Builder(applicationContext).build()
        playerView.player = player
        teleportSDK.setTeleportEventsListener(this)
    }

    override fun onStart() {
        super.onStart()
        try {
            //Start TeleportSDK before call player.start()
            teleportSDK.start()
        } catch (ex: InitializationException) {
            Log.e(TAG, "Init exception ${ex.message}")
        }

        teleportSDK.setBufferSizeGetter(object : BufferSizeGetter() {
            override fun looper(): Looper {
                return player.applicationLooper
            }

            override fun getBufferSize(): Long {
                return player.totalBufferedDuration.coerceAtLeast(0)
            }
        })

        // clear url. It`s uses for identify stream
        teleportSDK.setUrlCleaner { it.authority + it.path }

        // Set manifest acceptor. Uses for identify url of playlist
        teleportSDK.setManifestAcceptor { uri ->
            uri.toString().let { it.contains("mpd") || it.contains("m3u") }
        }

        val newUrl = teleportSDK.getChangedManifestUrl(urlToPlay)
        preparePlayer(newUrl)
    }

    override fun onDestroy() {
        super.onDestroy()
        teleportSDK.dispose()
        player.release()
    }

    private fun preparePlayer(uri: Uri) {
        val mediaItem: MediaItem = MediaItem.fromUri(uri)
        player.setMediaItem(mediaItem)
        buildMediaSource(uri)?.let { player.addMediaSource(it) }
        player.prepare()
        player.play()
    }

    private fun buildMediaSource(manifestUri: Uri): MediaSource? {
        var mediaSource: MediaSource? = null
        val factory: DataSource.Factory = DefaultHttpDataSource.Factory()
        val urlLastPathSegment = manifestUri.lastPathSegment
        if (urlLastPathSegment != null) {
            if (urlLastPathSegment.contains("m3u8")) {
                val hlsFactory = HlsMediaSource.Factory(factory)
                mediaSource = hlsFactory.createMediaSource(MediaItem.fromUri(manifestUri))
            } else if (urlLastPathSegment.contains("mpd")) {
                val dashFactory =
                    DashMediaSource.Factory(DefaultDashChunkSource.Factory(factory), factory)
                mediaSource = dashFactory.createMediaSource(MediaItem.fromUri(manifestUri))
            }
        }
        return mediaSource
    }


    @Deprecated("Deprecated in Java")
    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if (playbackState == PlaybackState.STATE_BUFFERING) {
            teleportSDK.buffering()
        }
    }

    // TeleportSdk listener events
    override fun onServerConnected(p0: String) {
        Log.d(TAG, "onServerConnected peerId=$p0")
    }

    override fun onSegmentLoaded(p0: DownloadStatSegment) {
        Log.d(TAG, "Loaded ${p0.result.segmentId} source=${p0.result.sourceId}")
    }

    override fun onSegmentUploaded(p0: UploadStatSegment) {
        Log.d(TAG, p0.toString())
    }

    override fun onPeerConnected(p0: String) {
        Log.d(TAG, "onPeerConnected peekId=$p0")
    }

    override fun onPeerDisconnected(p0: String) {
        Log.d(TAG, "onPeerDisconnected peerId=$p0")
    }

    override fun onPeeringModeChanged(p0: PeeringMode) {
        Log.d(TAG, "PeerModeChanged ${p0.name}")
    }

    override fun onError(p0: Throwable) {
        Log.e(TAG, "Error ${p0.message}")
    }
}